package io.mondegreen.demo

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.api.dagger.ApiModule
import io.mondegreen.photosplash.api.service.UnsplashService
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, SharedPreferencesModule::class, AuthenticationModule::class])
interface CoreComponent {

    @Component.Factory interface Factory {

        fun create(@BindsInstance context: Context): CoreComponent
    }

    // TODO: Find a way to get rid of this.
    fun provideUnsplashService(): UnsplashService
    fun provideAuthenticationStore(): AuthenticationTokenStore
    // Provide the context passed to the CoreComponent to satisfy Module dependencies.
    fun provideContext(): Context

    fun inject(activity: MainActivity)
}