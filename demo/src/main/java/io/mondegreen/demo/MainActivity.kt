package io.mondegreen.demo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.mondegreen.photosplash.api.ApiResponse
import io.mondegreen.photosplash.api.UnsplashApi
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.api.entities.collection.PostCollectionBody
import io.mondegreen.photosplash.api.entities.collection.PutCollectionBody
import io.mondegreen.photosplash.api.entities.photo.PutPhotoBody
import io.mondegreen.photosplash.api.service.UnsplashService
import io.mondegreen.photosplash.core.data.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val disposables = CompositeDisposable()

    @Inject
    lateinit var api: UnsplashApi

    @Inject
    lateinit var service: UnsplashService

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    @Inject
    lateinit var authenticationTokenStore: AuthenticationTokenStore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        coreComponent(this).inject(this)

        initClickListeners()
    }

    private fun setMeClickListeners() {

        buttonUseToken.setOnClickListener {
            Timber.v("Use bearer token")
            authenticationTokenStore.useBearerToken = true
        }

        buttonDontUseToken.setOnClickListener {
            Timber.v("Do not use bearer token")
            authenticationTokenStore.useBearerToken = false
        }

        buttonGetMe.setOnClickListener {

            disposables += service.getMe()
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    when (it) {

                        is ApiResponse.Success -> Timber.v(it.data.toString())
                        is ApiResponse.Error -> Timber.e(it.exception)
                    }
                    //Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem fetching me")
                })
        }

        buttonPutMe.setOnClickListener {

            disposables += api.putMe(
                mapOf(
                    "first_name" to "Sandertje",
                    "last_name" to "VH",
                    "bio" to "Fond of old timers, especially oldtimer mini bikes."
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem putting me")
                })
        }
    }

    private fun setUsersClickListeners() {

        buttonGetUser.setOnClickListener {

            disposables += api.getUser("harleydavidson")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {
                    Timber.e(it, "Problem when getUser()")
                })
        }

        buttonGetUserPhotos.setOnClickListener {

            disposables += api.listUserPhotos(
                "harleydavidson", mapOf(
                    "stats" to "true",
                    "per_page" to "200"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {
                    Timber.e(it, "Problem when listUserPhotos()")
                })
        }

        buttonGetUserLikedPhotos.setOnClickListener {

            disposables += api.listUserLikedPhotos(
                "harleydavidson", mapOf(
                    "per_page" to "200"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {
                    Timber.e(it, "Problem when listUserLikedPhotos()")
                })
        }

        buttonGetUserCollections.setOnClickListener {

            disposables += api.listUserCollections(
                "harleydavidson", mapOf(
                    "per_page" to "200"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {
                    Timber.e(it, "Problem when listUserCollections()")
                })
        }

        buttonGetUserStatistics.setOnClickListener {

            disposables += api.getUserStatistics(
                "harleydavidson", mapOf(
                    "per_page" to "200"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {
                    Timber.e(it, "Problem when getUserStatistics()")
                })
        }
    }

    private fun setPhotosClickListeners() {

        buttonGetPhotos.setOnClickListener {

            disposables += api.listPhotos(mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when listPhotos()")
                })
        }

        buttonGetPhoto.setOnClickListener {

            disposables += api.getPhoto("qntRxcShKi8")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when getPhoto()")
                })
        }

        buttonGetRandomPhoto.setOnClickListener {

            disposables += api.getRandomPhoto(mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when getRandomPhoto()")
                })
        }

        buttonGetPhotoStats.setOnClickListener {

            disposables += api.getPhotoStatistics("qntRxcShKi8", mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when getPhotoStatistics()")
                })
        }

        buttonTrackPhotoDownload.setOnClickListener {

            disposables += api.downloadPhoto("qntRxcShKi8")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when downloadPhoto()")
                })
        }

        buttonUpdatePhoto.setOnClickListener {

            disposables += api.putPhoto("qntRxcShKi8", PutPhotoBody())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when putPhoto()")
                })
        }

        buttonLikePhoto.setOnClickListener {

            disposables += api.likePhoto("qntRxcShKi8")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when likePhoto()")
                })
        }

        buttonDislikePhoto.setOnClickListener {

            disposables += api.unlikePhoto("qntRxcShKi8")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when unlikePhoto()")
                })
        }
    }

    private fun setSearchListeners() {

        buttonSearchUsers.setOnClickListener {

            disposables += api.searchUsers(
                mapOf(

                    "query" to "mustang"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when searchUsers()")
                })
        }

        buttonSearchPhotos.setOnClickListener {

            disposables += api.searchPhotos(
                mapOf(

                    "query" to "mustang"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when searchPhotos()")
                })
        }

        buttonSearchCollections.setOnClickListener {

            disposables += api.searchCollections(
                mapOf(

                    "query" to "mustang"
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when searchCollections()")
                })
        }
    }

    private fun setCollectionListeners() {

        buttonListCollections.setOnClickListener {

            disposables += api.listCollections(mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when listCollections()")
                })
        }

        buttonListFeaturedCollections.setOnClickListener {

            disposables += api.listFeaturedCollections(mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when listFeaturedCollections()")
                })
        }

        buttonGetCollection.setOnClickListener {

            disposables += api.getCollection("8450940")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when getCollection()")
                })
        }

        buttonGetCollectionPhotos.setOnClickListener {

            disposables += api.listCollectionPhotos("8450940", mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when listCollectionPhotos()")
                })
        }

        buttonGetCuratedCollectionPhotos.setOnClickListener {

            disposables += api.listCuratedCollectionPhotos("8450940", mapOf())
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when listCuratedCollectionPhotos()")
                })
        }

        buttonGetCollectionRelatedCollections.setOnClickListener {

            disposables += api.getRelatedCollections("8450940")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when getRelatedCollections()")
                })
        }

        buttonCreateCollection.setOnClickListener {

            disposables += api.postCollection(
                PostCollectionBody(
                    "phones",
                    "A collection containing pictures about phones", true
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when postCollection()")
                })
        }

        buttonUpdateCollection.setOnClickListener {

            disposables += api.putCollection(
                "8524829", PutCollectionBody(
                    "updated phones",
                    "We've just updated the phones collection", false
                )
            )
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when putCollection()")
                })
        }

        buttonDeleteCollection.setOnClickListener {

            disposables += api.deleteCollection("")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when deleteCollection()")
                })
        }

        buttonAddPhotoToCollection.setOnClickListener {

            disposables += api.addCollectionPhoto("8524829", "EOJqV9lZNDk")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when addCollectionPhoto()")
                })
        }

        buttonRemovePhotoFromCollection.setOnClickListener {

            disposables += api.deleteCollectionPhoto("8524829", "EOJqV9lZNDk")
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.ui)
                .subscribe({

                    Timber.v(it.body()?.toString())
                }, {

                    Timber.e(it, "Problem when deleteCollectionPhoto()")
                })
        }
    }

    private fun initClickListeners() {

        setMeClickListeners()
        setUsersClickListeners()
        setPhotosClickListeners()
        setSearchListeners()
        setCollectionListeners()
    }
}
