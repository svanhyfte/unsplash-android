package io.mondegreen.demo

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }

    private val coreComponent: CoreComponent by lazy {

        DaggerCoreComponent.factory().create(this)
    }

    companion object {
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as App).coreComponent
    }
}

fun FragmentActivity.coreComponent(context: Context) = App.coreComponent(context)

fun Fragment.coreComponent(context: Context) = App.coreComponent(context)