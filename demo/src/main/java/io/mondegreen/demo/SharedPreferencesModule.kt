package io.mondegreen.demo

import android.content.Context
import dagger.Module
import dagger.Provides
import io.mondegreen.photosplash.core.data.dagger.qualifiers.Authentication

private const val PREFS_AUTHENTICATION = "prefs_photosplash_authentication"

@Module
object SharedPreferencesModule {

    @Provides
    @JvmStatic
    @Authentication
    fun provideSharedPreferences(context: Context) =

        context.getSharedPreferences(PREFS_AUTHENTICATION, Context.MODE_PRIVATE)
}