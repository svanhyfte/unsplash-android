package io.mondegreen.demo

import dagger.Binds
import dagger.Module
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore

@Module
abstract class AuthenticationModule {

    @Binds
    abstract fun provideAuthStore(store: LocalAuthenticationTokenStore): AuthenticationTokenStore
}