package io.mondegreen.photosplash.core_android.utils

import android.content.Intent
import android.os.Bundle

private const val KEY_NAV_CONTROLLER_DEEPLINK_INTENT = "android-support-nav:controller:deepLinkIntent"

object DeeplinkUtils {

    fun getDeeplinkIntentFromArguments(arguments: Bundle): Intent? =
        arguments[KEY_NAV_CONTROLLER_DEEPLINK_INTENT] as? Intent
}
