package io.mondegreen.photosplash.core_android.extensions

import android.net.Uri
import java.net.URI

fun Uri?.toURI() = this?.let { URI.create(it.toString()) }
