package io.mondegreen.photosplash.core_android.views.toolbar

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import io.mondegreen.photosplash.core_android.R

class MenuBar : LinearLayout {

    constructor(context: Context?) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {

        init(context, attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        init(context, attrs, defStyleAttr)
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        super.onRestoreInstanceState(state)
    }

    override fun onSaveInstanceState(): Parcelable? {
        return super.onSaveInstanceState()
    }

    private fun init(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) {

        LayoutInflater.from(context).inflate(R.layout.view_menubar, this, true)
        orientation = HORIZONTAL
    }
}
