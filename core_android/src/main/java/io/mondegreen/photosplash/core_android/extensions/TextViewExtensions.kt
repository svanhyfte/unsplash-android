package io.mondegreen.photosplash.core_android.extensions

import android.os.Build
import android.widget.TextView

fun TextView.setCompatTextAppearance(textAppearanceStyle: Int) {

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

        this.setTextAppearance(context, textAppearanceStyle)
    } else {

        this.setTextAppearance(textAppearanceStyle)
    }
}
