package io.mondegreen.photosplash.core_android.extensions

import android.content.Context
import android.view.animation.AnimationUtils
import androidx.annotation.AnimRes

fun Context.loadAnimation(@AnimRes res: Int) = AnimationUtils.loadAnimation(this, res)
