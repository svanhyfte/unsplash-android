package io.mondegreen.photosplash.core_android.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

inline fun <reified T : ViewModel> Fragment.getViewModel(factory: ViewModelProvider.Factory): T =

    ViewModelProvider(this, factory).get(T::class.java)

inline fun <reified T : ViewModel> Fragment.getActivityViewModel(factory: ViewModelProvider.Factory): T =

    requireActivity().getViewModel(factory)
