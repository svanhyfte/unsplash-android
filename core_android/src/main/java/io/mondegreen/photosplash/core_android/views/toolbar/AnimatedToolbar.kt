package io.mondegreen.photosplash.core_android.views.toolbar

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import com.google.android.material.appbar.MaterialToolbar
import io.mondegreen.photosplash.core_android.R
import io.mondegreen.photosplash.core_android.extensions.loadAnimation
import io.mondegreen.photosplash.core_android.extensions.setCompatTextAppearance
import kotlinx.android.synthetic.main.view_toolbar.view.*

private const val TITLE_ANIMATION_DURATION_DEFAULT = 200

class AnimatedToolbar : MaterialToolbar {

    constructor(context: Context?) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {

        init(context, attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) {

        LayoutInflater.from(context).inflate(R.layout.view_toolbar, this, true)

        val styledAttributes = context?.obtainStyledAttributes(attrs, R.styleable.AnimatedToolbar)

        var titleTextAppearance = 0
        var titleAnimationDuration = 0L

        styledAttributes?.let {

            titleTextAppearance = it.getResourceId(R.styleable.AnimatedToolbar_titleTextStyle, 0)
            titleAnimationDuration = it.getInteger(R.styleable.AnimatedToolbar_titleAnimationDuration, TITLE_ANIMATION_DURATION_DEFAULT).toLong()
        }

        styledAttributes?.recycle()

        setupTitleTextSwitcher(titleTextAppearance, titleAnimationDuration)
    }

    override fun setTitle(resId: Int) {

        textSwitcherToolbarTitle.postDelayed({

            textSwitcherToolbarTitle.setText(context.getText(resId))
        }, 500)
    }

    override fun setTitle(title: CharSequence?) {

        textSwitcherToolbarTitle.postDelayed({

            textSwitcherToolbarTitle.setText(title)
        }, 500)
    }

    private fun setupTitleTextSwitcher(textAppearanceStyle: Int, animationDuration: Long) {
        textSwitcherToolbarTitle.setFactory {

            TextView(context!!).apply {

                setCompatTextAppearance(textAppearanceStyle)
                setSingleLine()
                ellipsize = TextUtils.TruncateAt.END
            }
        }

        textSwitcherToolbarTitle.inAnimation = context.loadAnimation(R.anim.fade_in).apply { duration = animationDuration }
        textSwitcherToolbarTitle.outAnimation = context.loadAnimation(R.anim.fade_out).apply { duration = animationDuration }
    }
}
