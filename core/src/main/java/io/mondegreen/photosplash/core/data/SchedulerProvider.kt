package io.mondegreen.photosplash.core.data

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SchedulerProvider(
    val ui: Scheduler,
    val io: Scheduler,
    val computation: Scheduler,
    val single: Scheduler,
    val newThread: Scheduler
) {

    @Inject
    constructor() : this(
        AndroidSchedulers.mainThread(),
        Schedulers.io(),
        Schedulers.computation(),
        Schedulers.single(),
        Schedulers.newThread()
    )
}
