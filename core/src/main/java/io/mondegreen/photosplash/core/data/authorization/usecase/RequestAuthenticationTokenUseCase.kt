package io.mondegreen.photosplash.core.data.authorization.usecase

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.model.AuthenticationToken
import io.mondegreen.photosplash.core.data.authorization.repository.AuthorizationRepository
import io.reactivex.Single
import javax.inject.Inject
import timber.log.Timber

class RequestAuthenticationTokenUseCase @Inject constructor(private val authorizationRepository: AuthorizationRepository) {

    operator fun invoke(request: RequestAuthenticationTokenRequest): Single<Result<AuthenticationToken>> {

        val token = request.token()

        return if (token.isNullOrEmpty()) {

            Single.error(IllegalArgumentException("No token was provided."))
        } else {

            authorizationRepository.requestAuthenticationToken(token)
                .doOnSuccess { Timber.v("Token received: $it") }
        }
    }
}
