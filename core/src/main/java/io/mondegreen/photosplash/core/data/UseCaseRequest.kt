package io.mondegreen.photosplash.core.data

/**
 * Base class for use case requests.
 */
open class UseCaseRequest {

    private val data = mutableMapOf<String, String>()

    fun add(key: String, value: String) {

        data[key] = value
    }

    fun get(key: String): String? = data[key]

    fun reset() {

        data.clear()
    }
}
