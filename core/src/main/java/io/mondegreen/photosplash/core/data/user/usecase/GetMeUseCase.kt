package io.mondegreen.photosplash.core.data.user.usecase

import io.mondegreen.photosplash.core.data.user.repository.UserRepository
import javax.inject.Inject

class GetMeUseCase @Inject constructor(private val userRepository: UserRepository) {

    operator fun invoke() = userRepository.getMe()
}
