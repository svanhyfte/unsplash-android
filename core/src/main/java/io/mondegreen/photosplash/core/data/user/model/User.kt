package io.mondegreen.photosplash.core.data.user.model

data class User(
    val id: String,
    val userName: String,
    val firstName: String?,
    val lastName: String?,
    val twitterName: String?,
    val portfolioUrl: String?,
    val bio: String?,
    val location: String?,

    val totalLikes: Int,
    val totalPhotos: Int,
    val totalCollections: Int,
    val isFollowedByUser: Boolean,
    val downloads: Int,
    val instagramUsername: String?,
    val email: String?,
    val links: Map<String, String>,
    val profileImages: Map<String, String>,
    val badge: Badge?
) {

    fun getLink(linkType: LinkType): String? = links[linkType.type]

    fun getProfileImage(imageType: UrlType): String? = profileImages[imageType.type]
}
