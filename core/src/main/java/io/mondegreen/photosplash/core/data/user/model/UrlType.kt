package io.mondegreen.photosplash.core.data.user.model

enum class UrlType(val type: String) {

    RAW("raw"),
    FULL("full"),
    REGULAR("regular"),
    SMALL("small"),
    THUMB("thumb"),
    MEDIUM("medium"),
    LARGE("large")
}
