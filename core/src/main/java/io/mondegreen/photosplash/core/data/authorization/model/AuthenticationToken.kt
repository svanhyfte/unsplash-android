package io.mondegreen.photosplash.core.data.authorization.model

data class AuthenticationToken(val token: String)
