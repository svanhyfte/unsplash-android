package io.mondegreen.photosplash.core.data.login.usecase

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.usecase.RequestAuthenticationTokenRequest
import io.mondegreen.photosplash.core.data.authorization.usecase.RequestAuthenticationTokenUseCase
import io.mondegreen.photosplash.core.data.user.model.User
import io.mondegreen.photosplash.core.data.user.usecase.GetMeUseCase
import io.reactivex.Single
import javax.inject.Inject

class AuthenticateGetMeUseCase @Inject constructor(
    private val requestAuthenticationTokenUseCase: RequestAuthenticationTokenUseCase,
    private val getMeUseCase: GetMeUseCase
) {

    operator fun invoke(request: RequestAuthenticationTokenRequest): Single<Result<User>> {

        return requestAuthenticationTokenUseCase.invoke(request)
            .flatMap { getMeUseCase() }
    }
}
