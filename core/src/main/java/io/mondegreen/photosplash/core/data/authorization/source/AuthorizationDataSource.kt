package io.mondegreen.photosplash.core.data.authorization.source

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.model.AuthenticationToken
import io.reactivex.Single

interface AuthorizationDataSource {

    fun requestAuthenticationToken(code: String): Single<Result<AuthenticationToken>>
}
