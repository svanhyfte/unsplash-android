package io.mondegreen.photosplash.core.data.authorization.repository

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.model.AuthenticationToken
import io.reactivex.Single

interface AuthorizationRepository {

    fun requestAuthenticationToken(code: String): Single<Result<AuthenticationToken>>

    fun getAuthorizationUrl(): String
}
