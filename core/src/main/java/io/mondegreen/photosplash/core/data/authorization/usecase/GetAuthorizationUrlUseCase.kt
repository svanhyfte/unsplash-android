package io.mondegreen.photosplash.core.data.authorization.usecase

import io.mondegreen.photosplash.core.data.authorization.repository.AuthorizationRepository
import javax.inject.Inject

class GetAuthorizationUrlUseCase @Inject constructor(private val authorizationRepository: AuthorizationRepository) {

    operator fun invoke() = authorizationRepository.getAuthorizationUrl()
}
