package io.mondegreen.photosplash.core.data.authorization.usecase

import io.mondegreen.photosplash.core.data.UseCaseRequest

private const val KEY_URI = "uri"

class DecodeOAuthCodeRequest private constructor() : UseCaseRequest() {
    fun uri() = get(KEY_URI)

    companion object {

        fun build(uri: String?) = DecodeOAuthCodeRequest().apply {

            uri?.let { add(KEY_URI, it) }
        }
    }
}
