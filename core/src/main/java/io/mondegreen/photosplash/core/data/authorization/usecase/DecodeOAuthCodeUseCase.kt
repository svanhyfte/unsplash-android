package io.mondegreen.photosplash.core.data.authorization.usecase

import android.net.Uri
import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.error.AuthorizationException
import io.mondegreen.photosplash.core.data.authorization.error.AuthorizationExceptionType
import javax.inject.Inject

private const val AUTH_CALLBACK_SCHEME = "photosplash"
private const val AUTH_CALLBACK_PATH = "auth"
private const val AUTH_CALLBACK_HOST = "applink"
private const val QUERY_PARAMETER_ERROR = "error"
private const val QUERY_PARAMETER_ERROR_VALUE_DENIED = "access_denied"
private const val QUERY_PARAMETER_ERROR_DESCRIPTION = "error_description"
private const val QUERY_PARAMETER_CODE = "code"

// TODO: Switch to a Java equivalent to not depend on Android.
class DecodeOAuthCodeUseCase @Inject constructor() {

    operator fun invoke(request: DecodeOAuthCodeRequest): Result<String> {

        val uri = try {
            Uri.parse(request.uri())
        } catch (ex: Exception) {

            null
        }

        if (uri == null || !uri.isOAuthCallback()) {

            return Result.Error(AuthorizationException(AuthorizationExceptionType.NO_URI))
        }

        // Decode the token.
        if (uri.queryParameterNames.contains(QUERY_PARAMETER_CODE)) {

            return Result.Success(uri.getQueryParameter(QUERY_PARAMETER_CODE)!!)
        } else if (uri.queryParameterNames.contains(QUERY_PARAMETER_ERROR) &&
            QUERY_PARAMETER_ERROR_VALUE_DENIED.equals(uri.getQueryParameter(QUERY_PARAMETER_ERROR), ignoreCase = true)) {

            return Result.Error(AuthorizationException(AuthorizationExceptionType.NO_ACCESS))
        }

        return Result.Error(IllegalArgumentException())
    }
}

fun Uri.isOAuthCallback() = AUTH_CALLBACK_SCHEME.equals(scheme, ignoreCase = true) &&
        AUTH_CALLBACK_HOST.equals(host, ignoreCase = true) &&
        pathSegments.contains(AUTH_CALLBACK_PATH)
