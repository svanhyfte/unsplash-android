package io.mondegreen.photosplash.core.data.dagger.scopes

import javax.inject.Scope

/**
 * Scope for a feature module.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FeatureScope
