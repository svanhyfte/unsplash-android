package io.mondegreen.photosplash.core.data.user.source

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.user.model.User
import io.reactivex.Single

interface UserDataSource {

    fun getMe(): Single<Result<User>>

    fun getUser(username: String): Single<Result<User>>
}
