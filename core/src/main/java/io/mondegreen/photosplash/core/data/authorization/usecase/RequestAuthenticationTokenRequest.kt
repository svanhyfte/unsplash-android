package io.mondegreen.photosplash.core.data.authorization.usecase

import io.mondegreen.photosplash.core.data.UseCaseRequest

private const val KEY_TOKEN = "token"

class RequestAuthenticationTokenRequest private constructor() : UseCaseRequest() {

    fun token() = get(KEY_TOKEN)

    companion object {

        fun build(token: String) = RequestAuthenticationTokenRequest().apply {

            add(KEY_TOKEN, token)
        }
    }
}
