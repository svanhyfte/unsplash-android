package io.mondegreen.photosplash.core.data.user.model

data class Badge(
    val title: String,
    val isPrimary: Boolean,
    val slug: String,
    val link: String
)
