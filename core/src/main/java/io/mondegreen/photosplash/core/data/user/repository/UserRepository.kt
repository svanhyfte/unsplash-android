package io.mondegreen.photosplash.core.data.user.repository

import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.user.model.User
import io.reactivex.Single

interface UserRepository {

    fun getMe(): Single<Result<User>>

    fun getUser(username: String): Single<Result<User>>
}
