package io.mondegreen.photosplash.core.data.user.model

enum class LinkType(val type: String) {

    SELF("self"),
    HTML("html"),
    PHOTOS("photos"),
    LIKES("likes"),
    PORTFOLIO("portfolio"),
    FOLLOWERS("followers"),
    FOLLOWING("following"),

    DOWNLOAD("download"),
    DOWNLOAD_LOCATION("download_location")
}
