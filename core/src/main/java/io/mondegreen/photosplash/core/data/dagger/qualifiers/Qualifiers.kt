package io.mondegreen.photosplash.core.data.dagger.qualifiers

import javax.inject.Qualifier

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class Authentication
