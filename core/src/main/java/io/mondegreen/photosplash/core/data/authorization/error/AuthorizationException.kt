package io.mondegreen.photosplash.core.data.authorization.error

class AuthorizationException(val type: AuthorizationExceptionType) : Exception("OAuth url could not be decoded properly.")

enum class AuthorizationExceptionType {

    NO_URI,
    NO_CODE,
    NO_ACCESS
}
