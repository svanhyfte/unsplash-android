package io.mondegreen.photosplash.api.entities.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.photo.PhotoResponse

@JsonClass(generateAdapter = true)
data class SearchPhotoResponse(
    @Json(name = "total") val total: Int,
    @Json(name = "total_pages") val totalPages: Int,
    @Json(name = "results") val results: List<PhotoResponse> = listOf()
)
