package io.mondegreen.photosplash.api.interceptors

import javax.inject.Inject
import okhttp3.Interceptor
import okhttp3.Response

private const val HEADER_ACCEPT_VERSION = "Accept-Version"

internal class DefaultHeaderInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        return chain.proceed(chain.request()
            .newBuilder()
            .addHeader(HEADER_ACCEPT_VERSION, "v1")
            .build())
    }
}
