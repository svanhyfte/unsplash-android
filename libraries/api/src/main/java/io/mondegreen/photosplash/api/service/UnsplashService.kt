package io.mondegreen.photosplash.api.service

import io.mondegreen.photosplash.api.UnsplashApi
import io.mondegreen.photosplash.api.convertToApiResponse
import io.mondegreen.photosplash.api.error.ErrorFactory

// The main entry point for other classes to interact with the Unsplash API.
class UnsplashService internal constructor(
    private val unsplashApi: UnsplashApi,
    private val errorFactory: ErrorFactory
) {

    fun getAuthorizationToken(
        url: String,
        clientId: String,
        clientSecret: String,
        redirectUri: String,
        code: String,
        grantType: String
    ) =
        unsplashApi.requestOAuthToken(url,
            clientId,
            clientSecret,
            redirectUri,
            code,
            grantType).map { it.convertToApiResponse(errorFactory) }

    //region Users

    fun getMe() = unsplashApi.getMe().map { it.convertToApiResponse(errorFactory) }

    fun putMe(data: Map<String, String>) = unsplashApi.putMe(data).map { it.convertToApiResponse(errorFactory) }

    fun getUser(username: String) = unsplashApi.getUser(username).map { it.convertToApiResponse(errorFactory) }

    fun getUserPhotos(username: String, filters: Map<String, String>) = unsplashApi.listUserPhotos(username, filters).map { it.convertToApiResponse(errorFactory) }

    //endregion

    //region Photos

    fun getPhoto(id: String) = unsplashApi.getPhoto(id).map { it.convertToApiResponse(errorFactory) }

    //endregion
}
