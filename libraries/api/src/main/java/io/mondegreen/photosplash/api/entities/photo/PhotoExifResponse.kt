package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoExifResponse(
    @Json(name = "make") val make: String,
    @Json(name = "model") val model: String,
    @Json(name = "exposure_time") val exposureTime: String,
    @Json(name = "aperture") val aperture: String,
    @Json(name = "iso") val iso: Int
)
