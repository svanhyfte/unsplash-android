package io.mondegreen.photosplash.api.entities.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.photo.TagResponse

@JsonClass(generateAdapter = true)
data class TagsResponse(
    @Json(name = "custom") val customTags: List<TagResponse> = listOf(),
    @Json(name = "aggregated") val aggregatedTags: List<TagResponse> = listOf()
)
