package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.collection.CollectionResponse
import io.mondegreen.photosplash.api.entities.user.UserResponse

@JsonClass(generateAdapter = true)
data class CurrentUserCollectionResponse(
    @Json(name = "id") val id: String,
    @Json(name = "title") val title: String,
    @Json(name = "description") val description: String?,
    @Json(name = "published_at") val publicationDate: String,
    @Json(name = "updated_at") val updateDate: String,
    @Json(name = "curated") val curated: Boolean,
    @Json(name = "featured") val featured: Boolean,
    @Json(name = "total_photos") val totalPhotos: Int,
    @Json(name = "private") val isPrivate: Boolean,
    @Json(name = "share_key") val shareKey: String,
    @Json(name = "tags") val tags: List<TagResponse>
)

@JsonClass(generateAdapter = true)
data class PhotoResponse(
    @Json(name = "id") val id: String,
    @Json(name = "created_at") val creationDate: String,
    @Json(name = "updated_at") val updateDate: String,
    @Json(name = "width") val width: Int,
    @Json(name = "height") val height: Int,
    @Json(name = "color") val color: String,
    @Json(name = "description") val description: String?,
    @Json(name = "alt_description") val alternativeDescription: String?,
    // TODO: Map to Map instead of object.
    @Json(name = "urls") val urls: Map<String, String>,
    @Json(name = "links") val links: Map<String, String>,
    @Json(name = "categories") val categories: List<String> = listOf(),
    @Json(name = "likes") val likes: Int,
    @Json(name = "liked_by_user") val likedByUser: Boolean,
    // Optional. Only in detail.
    @Json(name = "downloads") val downloads: Int = 0,
    // Optional. Only for detail.
    @Json(name = "exif") val exif: PhotoExifResponse? = null,
    // Optional. Only for detail.
    @Json(name = "location") val location: PhotoLocationResponse? = null,
    // Optional. Only for detail.
    @Json(name = "tags") val tags: List<TagResponse> = listOf(),
    // Optional. Only for detail.
    @Json(name = "user") val user: UserResponse?,
    @Json(name = "sponsorship") val sponshorship: PhotoSponsorshipResponse?,
    @Json(name = "statistics") val stats: StatisticsResponse?,
    @Json(name = "current_user_collections") val currentUserCollections: List<CurrentUserCollectionResponse> = listOf(),
    @Json(name = "related_collections") val relatedCollections: RelatedCollectionsResponse?
)

@JsonClass(generateAdapter = true)
data class RelatedCollectionsResponse(
    @Json(name = "results") val results: List<CollectionResponse> = listOf()
)
