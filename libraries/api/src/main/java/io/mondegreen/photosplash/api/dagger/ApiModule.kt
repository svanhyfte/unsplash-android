package io.mondegreen.photosplash.api.dagger

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import dagger.Module
import dagger.Provides
import io.mondegreen.photosplash.api.BuildConfig
import io.mondegreen.photosplash.api.UnsplashApi
import io.mondegreen.photosplash.api.error.ErrorFactory
import io.mondegreen.photosplash.api.error.UnsplashErrorBody
import io.mondegreen.photosplash.api.interceptors.AuthenticationInterceptor
import io.mondegreen.photosplash.api.interceptors.DefaultHeaderInterceptor
import io.mondegreen.photosplash.api.service.UnsplashService
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Dagger Module for the Api Android module.
 *
 * Since we only want to expose the Api object, the other dependencies are created statically,
 * and not resolved by Dagger.
 */
@Module
object ApiModule {

    @JvmStatic
    @Provides
    internal fun provideOkHttpClient(
        authenticationInterceptor: AuthenticationInterceptor,
        defaultHeaderInterceptor: DefaultHeaderInterceptor
    ) =
        OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(authenticationInterceptor)
            .addInterceptor(defaultHeaderInterceptor)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            })
            .build()

    // TODO: Add necessary converter adapters
    // TODO: Do not expose.
    @JvmStatic
    internal fun provideMoshi() = Moshi.Builder()
        .build()

    @JvmStatic
    private fun provideErrorFactory(moshi: Moshi): ErrorFactory {

        val type = Types.newParameterizedType(UnsplashErrorBody::class.java)
        val adapter = moshi.adapter<UnsplashErrorBody>(type)

        return ErrorFactory(adapter)
    }

    @JvmStatic
    private fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi) =
        Retrofit.Builder()
            .baseUrl(BuildConfig.UNSPLASH_API_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

    @Provides
    @JvmStatic
    @Singleton
    // TODO: Do not expose.
    internal fun provideUnsplashApi(okHttpClient: OkHttpClient): UnsplashApi {

        val moshi = provideMoshi()

        val api = provideRetrofit(
            okHttpClient,
            moshi
        ).create(UnsplashApi::class.java)

        return api
    }

    @Provides
    @JvmStatic
    @Singleton
    internal fun provideUnsplashService(api: UnsplashApi): UnsplashService {

        val moshi = provideMoshi()

        val errorFactory = provideErrorFactory(moshi)

        return UnsplashService(api, errorFactory)
    }
}
