package io.mondegreen.photosplash.api.adapters

import com.squareup.moshi.JsonQualifier

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
internal annotation class JsonDate
