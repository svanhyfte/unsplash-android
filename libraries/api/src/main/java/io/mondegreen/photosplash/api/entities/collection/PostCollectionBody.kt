package io.mondegreen.photosplash.api.entities.collection

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PostCollectionBody(
    @Json(name = "title") val title: String,
    @Json(name = "description") val description: String? = null,
    @Json(name = "private") val isPrivate: Boolean? = null
)
