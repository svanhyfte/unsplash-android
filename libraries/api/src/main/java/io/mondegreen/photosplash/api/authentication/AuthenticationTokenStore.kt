package io.mondegreen.photosplash.api.authentication

interface AuthenticationTokenStore {

    var bearerAccessToken: String?

    var useBearerToken: Boolean

    fun clearData()
}
