package io.mondegreen.photosplash.api.entities.collection

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.photo.PhotoResponse
import io.mondegreen.photosplash.api.entities.user.UserResponse

/**
 * Response returned from endpoints that perform actions on collections: adding and removing a photo.
 */
@JsonClass(generateAdapter = true)
data class PhotoToCollectionActionResponse(
    @Json(name = "photo") val photo: PhotoResponse,
    @Json(name = "collection") val collection: CollectionResponse,
    @Json(name = "user") val user: UserResponse,
    @Json(name = "created_at") val creationDate: String
)
