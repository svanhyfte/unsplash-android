package io.mondegreen.photosplash.api.interceptors

import io.mondegreen.photosplash.api.BuildConfig
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.api.authorization.AuthorizationStore
import javax.inject.Inject
import okhttp3.Interceptor
import timber.log.Timber

private const val HEADER_AUTHORIZATION = "Authorization"

/**
 * An interceptor that either adds the application's client id or the user's bearer token to the request headers.
 */
internal class AuthenticationInterceptor @Inject constructor(
    private val authenticationTokenStore: AuthenticationTokenStore,
    private val authorizationStore: AuthorizationStore
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain) = chain.proceed(
        chain.request()
            .newBuilder()
            .addHeader(
                HEADER_AUTHORIZATION, if (
                // !authenticationTokenStore.bearerAccessToken.isNullOrEmpty() &&
                    authenticationTokenStore.useBearerToken) {

                    // "Bearer ${authenticationTokenStore.bearerAccessToken}"
                    // TODO: Remove again once API test is completed.
                    "Bearer ${BuildConfig.UNSPLASH_BEARER_TOKEN}"
                } else {

                    "Client-ID ${authorizationStore.clientId}"
                }
            )
            .build().also {

                Timber.v("Token being used: ${it.header(HEADER_AUTHORIZATION)}")
            }
    )
}
