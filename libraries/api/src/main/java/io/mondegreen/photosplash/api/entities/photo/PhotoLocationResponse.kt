package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoLocationResponse(
    @Json(name = "title") val title: String,
    @Json(name = "name") val name: String?,
    @Json(name = "city") val city: String?,
    @Json(name = "country") val country: String?,
    @Json(name = "position") val position: PhotoLocationPositionResponse?
)
