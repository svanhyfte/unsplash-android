package io.mondegreen.photosplash.api.error

import com.squareup.moshi.JsonAdapter
import okhttp3.ResponseBody
import retrofit2.Response
import timber.log.Timber

private const val HEADER_RATE_LIMIT_REMAINING = "x-ratelimit-remaining"

internal class ErrorFactory(private val jsonAdapter: JsonAdapter<UnsplashErrorBody>) {

    fun produceDefaultError() = UnsplashServerError(ErrorCode.UNKNOWN)

    fun <T> processResponse(response: Response<T>): Exception {

        val responseCode = response.code()

        val rateLimitRemaining = response.headers().get(HEADER_RATE_LIMIT_REMAINING)?.toInt() ?: -1

        val error = if (responseCode == ErrorCode.FORBIDDEN.code && rateLimitRemaining == 0) {

            UnsplashRateLimitExceededError(code = ErrorCode.FORBIDDEN)
        } else {

            response.errorBody()?.let {

                processHttpErrorBody(responseCode, it)
            } ?: produceDefaultError()
        }

        Timber.e(error)

        return error
    }

    private fun processHttpErrorBody(code: Int, responseBody: ResponseBody): Exception {

        val errors = decodeErrorsFromErrorBody(responseBody)

        val errorCode = ErrorCode.values().find { it.code == code } ?: ErrorCode.UNKNOWN

        return UnsplashServerError(errorCode, errors)
    }

    // TODO: Peak body to make sure an errors array is available.
    private fun decodeErrorsFromErrorBody(responseBody: ResponseBody): List<String> = try {

        jsonAdapter.fromJson(responseBody.string())?.errors ?: listOf()
    } catch (exception: Exception) {

        Timber.e(exception)
        listOf()
    }
}
