package io.mondegreen.photosplash.api.entities.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.collection.PreviewPhotoResponse

@JsonClass(generateAdapter = true)
data class UserResponse(
    @Json(name = "id") val id: String,
    // TODO: Process to date using an adapter
    @Json(name = "updated_at") val updateDate: String,
    @Json(name = "username") val username: String,
    @Json(name = "name") val name: String?,
    @Json(name = "first_name") val firstName: String?,
    @Json(name = "last_name") val lastName: String?,
    @Json(name = "twitter_username") val twitterName: String?,
    @Json(name = "instagram_username") val instagramUsername: String?,
    @Json(name = "portfolio_url") val portfolioUrl: String?,
    @Json(name = "bio") val bio: String?,
    @Json(name = "location") val location: String?,
    @Json(name = "total_likes") val totalLikes: Int,
    @Json(name = "total_photos") val totalPhotos: Int,
    @Json(name = "total_collections") val totalCollections: Int,
    // Optional
    @Json(name = "followers_count") val followersCount: Int = 0,
    // Optional
    @Json(name = "following_count") val followingCount: Int = 0,
    @Json(name = "followed_by_user") val followedByUser: Boolean = false,
    @Json(name = "accepted_tos") val hasAcceptedTos: Boolean,
    @Json(name = "downloads") val downloads: Int = 0,
    @Json(name = "email") val email: String?,
    // TODO: links & profile are objects, convert them to a map of properties.
    @Json(name = "links") val links: Map<String, String>,
    @Json(name = "profile_image") val profileImages: Map<String, String>,
    @Json(name = "badge") val badge: BadgeResponse?,
    @Json(name = "photos") val photos: List<PreviewPhotoResponse> = listOf(),
    @Json(name = "tags") val tags: TagsResponse? = null
)
