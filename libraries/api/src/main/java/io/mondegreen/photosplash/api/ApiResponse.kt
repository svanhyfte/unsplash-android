package io.mondegreen.photosplash.api

import io.mondegreen.photosplash.api.error.ErrorFactory
import retrofit2.Response

/**
 * Wrapper around Retrofit's ResponseBody.
 */
sealed class ApiResponse<out T : Any>() {

    abstract val isSuccess: Boolean

    data class Success<out T : Any>(val data: T) : ApiResponse<T>() {

        override val isSuccess: Boolean
            get() = true
    }

    data class Error(val exception: Exception) : ApiResponse<Nothing>() {

        override val isSuccess: Boolean
            get() = false
    }

    override fun toString(): String {

        return when (this) {

            is Success<*> -> "ApiResponse.Success[data=$data]"

            is Error -> "ApiResponse.Error[exception=$exception]"
        }
    }
}

internal inline fun <reified T : Any> Response<T>.convertToApiResponse(errorFactory: ErrorFactory): ApiResponse<T> {

    if (this.isSuccessful) {

        val data = this.body()

        if (data != null)

            return ApiResponse.Success(data)
    } else {

        return ApiResponse.Error(errorFactory.processResponse(this))
    }

    return ApiResponse.Error(errorFactory.produceDefaultError())
}
