package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PutPhotoBody(
    @Json(name = "location") val location: PutLocationBody? = null,
    @Json(name = "exif") val exif: PutExifBody? = null
)

@JsonClass(generateAdapter = true)
class PutLocationBody(
    @Json(name = "latitude") val latitude: String? = null,
    @Json(name = "longitude") val longitude: String? = null,
    @Json(name = "name") val name: String? = null,
    @Json(name = "city") val city: String? = null,
    @Json(name = "country") val country: String? = null,
    @Json(name = "confidential") val confidential: Boolean? = null
)

@JsonClass(generateAdapter = true)
class PutExifBody(
    @Json(name = "make") val make: String? = null,
    @Json(name = "model") val model: String? = null,
    @Json(name = "exposure_time") val exposureTime: String? = null,
    @Json(name = "aperture_value") val apertureValue: String? = null,
    @Json(name = "focal_length") val focalLength: String? = null,
    @Json(name = "iso_speed_ratings") val isoSpeedRatings: String? = null
)
