package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StatisticsResponse(
    @Json(name = "downloads") val downloads: StatisticResponse,
    @Json(name = "views") val views: StatisticResponse,
    @Json(name = "likes") val likes: StatisticResponse
)

@JsonClass(generateAdapter = true)
data class StatisticResponse(
    @Json(name = "total") val total: Int = 0,
    @Json(name = "historical") val historical: HistoricalStatisticResponse
)

@JsonClass(generateAdapter = true)
data class HistoricalStatisticResponse(
    @Json(name = "change") val change: Int = 0,
    @Json(name = "resolution") val resolution: String,
    @Json(name = "average") val average: Int = 0,
    @Json(name = "quantity") val quantity: Int = 0,
    @Json(name = "values") val values: List<HistoricalDayStatResponse> = listOf()
)

@JsonClass(generateAdapter = true)
data class HistoricalDayStatResponse(
    @Json(name = "date") val date: String,
    @Json(name = "value") val value: Int = 0
)
