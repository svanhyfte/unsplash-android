package io.mondegreen.photosplash.api.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

internal class DateAdapter {

    private val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault())

    @FromJson
    fun fromJson(@JsonDate date: String?): Date? {

        return date?.let { format.parse(it) }
    }

    @ToJson @JsonDate
    fun toJson(date: Date?): String? {

        return date?.let { format.format(it) }
    }
}
