package io.mondegreen.photosplash.api.error

import com.squareup.moshi.Json

internal class UnsplashErrorBody(
    @Json(name = "errors") val errors: List<String> = listOf()
)
