package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StoryResponse(
    @Json(name = "title") val title: String?,
    @Json(name = "description") val description: String?
)
