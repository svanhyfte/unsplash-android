package io.mondegreen.photosplash.api.error

/**
 * Wrapper class around the errors that can be returned from the API
 */
open class UnsplashServerError(
    open val code: ErrorCode,
    val errors: List<String> = listOf(),
    message: String? = null,
    throwable: Throwable? = null
) : Exception(message, throwable) {

    override fun toString(): String {
        return """Code: $code, Errors: $errors, Message: $message""".trimMargin()
    }
}

class UnsplashRateLimitExceededError(override val code: ErrorCode) :
    UnsplashServerError(code = code, message = "Rate limit exceeded")

enum class ErrorCode(val code: Int) {

    BAD_REQUEST(400),
    // Requires user authorisation
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    UNPROCESSABLE(422),

    UNKNOWN(-1)
}
