package io.mondegreen.photosplash.api.entities.collection

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.photo.PhotoResponse
import io.mondegreen.photosplash.api.entities.photo.TagResponse
import io.mondegreen.photosplash.api.entities.user.UserResponse

@JsonClass(generateAdapter = true)
data class CollectionResponse(
    @Json(name = "id") val id: String,
    @Json(name = "title") val title: String,
    @Json(name = "published_at") val publishDate: String,
    @Json(name = "updated_at") val updateDate: String,
    @Json(name = "description") val description: String?,
    @Json(name = "curated") val curated: Boolean,
    @Json(name = "featured") val featured: Boolean,
    @Json(name = "total_photos") val totalPhotos: Int,
    @Json(name = "private") val private: Boolean,
    @Json(name = "share_key") val shareKey: String,
    @Json(name = "cover_photo") val coverPhoto: PhotoResponse?,
    @Json(name = "links") val links: Map<String, String>,
    @Json(name = "user") val user: UserResponse,
    @Json(name = "preview_photos") val previewPhotos: List<PreviewPhotoResponse>? = listOf(),
    @Json(name = "tags") val tags: List<TagResponse> = listOf()
)

@JsonClass(generateAdapter = true)
data class PreviewPhotoResponse(
    @Json(name = "id") val id: String,
    @Json(name = "urls") val urls: Map<String, String> = mapOf()
)
