package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.mondegreen.photosplash.api.entities.user.UserResponse

@JsonClass(generateAdapter = true)
data class PhotoSponsorshipResponse(
    @Json(name = "impressions_id") val impressionsId: String,
    @Json(name = "tagline") val tagline: String?,
    @Json(name = "sponsor") val user: UserResponse?
)
