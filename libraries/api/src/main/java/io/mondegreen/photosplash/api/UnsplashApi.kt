package io.mondegreen.photosplash.api

import io.mondegreen.photosplash.api.entities.AuthenticationResponse
import io.mondegreen.photosplash.api.entities.collection.CollectionResponse
import io.mondegreen.photosplash.api.entities.collection.PhotoToCollectionActionResponse
import io.mondegreen.photosplash.api.entities.collection.PostCollectionBody
import io.mondegreen.photosplash.api.entities.collection.PutCollectionBody
import io.mondegreen.photosplash.api.entities.collection.SearchCollectionResponse
import io.mondegreen.photosplash.api.entities.photo.PhotoResponse
import io.mondegreen.photosplash.api.entities.photo.PutPhotoBody
import io.mondegreen.photosplash.api.entities.photo.SearchUserResponse
import io.mondegreen.photosplash.api.entities.photo.StatisticsResponse
import io.mondegreen.photosplash.api.entities.photo.UrlResponse
import io.mondegreen.photosplash.api.entities.user.SearchPhotoResponse
import io.mondegreen.photosplash.api.entities.user.UserResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap
import retrofit2.http.Url

// Interface for interacting with the Unsplash.com API.
interface UnsplashApi {

    //region authentication

    @POST
    fun requestOAuthToken(
        @Url url: String,
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("redirect_uri") redirectUri: String,
        @Query("code") code: String,
        @Query("grant_type") grantType: String
    ): Single<Response<AuthenticationResponse>>

    //endregion

    //region user

    @GET("me")
    fun getMe(): Single<Response<UserResponse>>

    /**
     * Use to update the user's details.
     *
     * Parameters:
     *
     * - username (o)
     * - first_name (o)
     * - last_name (o)
     * - email (o)
     * - url (o)
     * - location (o)
     * - bio (o)
     * - instagram_username (o)
     */
    @PUT("me")
    fun putMe(@Body body: Map<String, String>): Single<Response<UserResponse>>

    @GET("users/{username}")
    fun getUser(@Path("username") username: String): Single<Response<UserResponse>>

    /**
     * List a user's photos
     *
     * Parameters:
     *
     * - username (r)
     * - page (o)
     * - per_page (o)
     * - order_by (o): "latest", "oldest", "popular"
     * - stats (o): true | false
     * - resolution (o): "days"
     * - quantity (o)
     */
    @GET("users/{username}/photos")
    fun listUserPhotos(
        @Path("username") username: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<PhotoResponse>>>

    /**
     * List a user's liked photos
     *
     * Parameters:
     *
     * - username (r)
     * - page (o)
     * - per_page (o)
     * - order_by (o): "latest", "oldest", "popular"
     */
    @GET("users/{username}/photos")
    fun listUserLikedPhotos(
        @Path("username") username: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<PhotoResponse>>>

    /**
     * List a user's photos
     *
     * Parameters:
     *
     * - username (r)
     * - page (o)
     * - per_page (o)
     */
    @GET("users/{username}/collections")
    fun listUserCollections(
        @Path("username") username: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<CollectionResponse>>>

    /**
     * List a user's statistics
     *
     * Parameters:
     *
     * - username (r)
     * - resolution (o)
     * - quantity (o)
     */
    @GET("users/{username}/statistics")
    fun getUserStatistics(
        @Path("username") username: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<StatisticsResponse>>

    //endregion

    //region photos

    /**
     * List photos
     *
     * Parameters:
     *
     * - per_page (o)
     * - page (o)
     * - order_by (o): "latest", "oldest", "popular"
     */
    @GET("photos")
    fun listPhotos(
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<PhotoResponse>>>

    /**
     * Get a photo by its id.
     *
     * Parameters:
     * - id (r)
     */
    @GET("photos/{id}")
    fun getPhoto(
        @Path("id") id: String
    ): Single<Response<PhotoResponse>>

    /**
     * Get random photo
     *
     * Parameters:
     *
     * - collections (o): comma-separated ids of collections
     * - featured (o): true | false
     * - username (o)
     * - query (o)
     * - orientation (o): "landscape", "portrait", "squarish"
     * - count (o)
     */
    @GET("photos/random")
    fun getRandomPhoto(
        @QueryMap filters: Map<String, String>
    ): Single<Response<PhotoResponse>>

    /**
     * Get a photo's statistics.
     *
     * Parameters:
     *
     * - id (r)
     * - resolution (o): "days"
     * - quantity (o)
     */
    @GET("photos/{id}/statistics")
    fun getPhotoStatistics(
        @Path("id") id: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<StatisticsResponse>>

    /**
     * Download a photo.
     *
     * Parameters:
     *
     * - id (r)
     */
    @GET("photos/{id}/download")
    fun downloadPhoto(
        @Path("id") id: String
    ): Single<Response<UrlResponse>>

    // TODO: Convert the Map to just the Photo response by getting the "photo" key.
    @PUT("photos/{id}")
    fun putPhoto(
        @Path("id") id: String,
        @Body body: PutPhotoBody
    ): Single<Response<Map<String, PhotoResponse>>>

    // TODO: Convert to Response<Boolean>
    @POST("photos/{id}/like")
    fun likePhoto(@Path("id") id: String): Single<Response<Map<String, PhotoResponse>>>

    // TODO: Convert to Response<Boolean>
    @DELETE("photos/{id}/like")
    fun unlikePhoto(@Path("id") id: String): Single<Response<Map<String, PhotoResponse>>>

    //endregion

    //region search

    /**
     * Search photos.
     *
     * Parameters:
     *
     * - query (r)
     * - page (o)
     * - per_page (o)
     * - collections (o)
     * - orientation (o): "landscape", "portrait", "squarish"
     */
    @GET("search/photos")
    fun searchPhotos(@QueryMap filters: Map<String, String>): Single<Response<SearchPhotoResponse>>

    /**
     * Search collections.
     *
     * Parameters:
     *
     * - query (r)
     * - page (o)
     * - per_page (o)
     */
    @GET("search/collections")
    fun searchCollections(@QueryMap filters: Map<String, String>): Single<Response<SearchCollectionResponse>>

    /**
     * Search collections.
     *
     * Parameters:
     *
     * - query (r)
     * - page (o)
     * - per_page (o)
     */
    @GET("search/users")
    fun searchUsers(@QueryMap filters: Map<String, String>): Single<Response<SearchUserResponse>>

    //endregion

    //region Collections

    /**
     * Get collections.
     *
     * Parameters:
     *
     * - page (o)
     * - per_Page (o)
     */
    @GET("collections")
    fun listCollections(@QueryMap filters: Map<String, String>): Single<Response<List<CollectionResponse>>>

    /**
     * Get featured collections.
     *
     * Parameters:
     *
     * - page (o)
     * - per_Page (o)
     */
    @GET("collections/featured")
    fun listFeaturedCollections(@QueryMap filters: Map<String, String>): Single<Response<List<CollectionResponse>>>

    /**
     * Get collection.
     *
     * Parameters:
     *
     * - id (r)
     */
    @GET("collections/{id}")
    fun getCollection(@Path("id") id: String): Single<Response<CollectionResponse>>

    /**
     * List a collection's photos
     *
     * Parameters:
     *
     * - id (r)
     * - page (o)
     * - per_page (o)
     */
    @GET("collections/{id}/photos")
    fun listCollectionPhotos(
        @Path("id") id: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<PhotoResponse>>>

    /**
     * List a curated collection's photos
     *
     * Parameters:
     *
     * - id (r)
     * - page (o)
     * - per_page (o)
     */
    @GET("collections/curated/{id}/photos")
    fun listCuratedCollectionPhotos(
        @Path("id") id: String,
        @QueryMap filters: Map<String, String>
    ): Single<Response<List<PhotoResponse>>>

    /**
     * Get related collections.
     *
     * Parameters:
     *
     * - id (r)
     */
    @GET("collections/{id}/related")
    fun getRelatedCollections(@Path("id") id: String): Single<Response<List<CollectionResponse>>>

    /**
     * Create a collection.
     *
     * Parameters:
     *
     * - body
     */
    @POST("collections")
    fun postCollection(@Body body: PostCollectionBody): Single<Response<CollectionResponse>>

    /**
     * Update a collection.
     *
     * Parameters:
     *
     * - id
     * - body
     */
    @PUT("collections/{id}")
    fun putCollection(
        @Path("id") id: String,
        @Body body: PutCollectionBody
    ): Single<Response<CollectionResponse>>

    /**
     * Delete a collection.
     *
     * Parameters:
     *
     * - id
     */
    // TODO: Convert to Response<Boolean>
    @DELETE("collections/{id}")
    fun deleteCollection(@Path("id") id: String): Single<Response<Any>>

    /**
     * Add photo to a collection.
     *
     * Parameters:
     *
     * - collection_id
     * - photo_id
     */
    @POST("collections/{collection_id}/add")
    fun addCollectionPhoto(
        @Path("collection_id") collectionId: String,
        @Query("photo_id") photoId: String
    ): Single<Response<PhotoToCollectionActionResponse>>

    /**
     * Add photo to a collection.
     *
     * Parameters:
     *
     * - collection_id
     * - photo_id
     */
    @DELETE("collections/{collection_id}/remove")
    fun deleteCollectionPhoto(
        @Path("collection_id") collectionId: String,
        @Query("photo_id") photoId: String
    ): Single<Response<PhotoToCollectionActionResponse>>

    //endregion
}
