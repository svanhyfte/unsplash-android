package io.mondegreen.photosplash.api.entities.photo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoLocationPositionResponse(
    @Json(name = "longitude") val longitude: String?,
    @Json(name = "latitude") val latitude: String?
)
