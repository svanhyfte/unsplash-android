package io.mondegreen.photosplash.api.authorization

import io.mondegreen.photosplash.api.BuildConfig
import javax.inject.Inject

private val SCOPES = listOf(
    "public",
    "read_user",
    "write_user",
    "read_photos",
    "write_photos",
    "write_likes",
    "write_followers",
    "read_collections",
    "write_collections"
)

private const val AUTHORIZATION_URL = BuildConfig.UNSPLASH_AUTHORIZATION_URL
private const val OAUTH_URL = BuildConfig.UNSPLASH_OAUTH_URL

private const val RESPONSE_TYPE = "code"

class AuthorizationStore @Inject constructor() {

    val oAuthTokenUrl = OAUTH_URL

    val clientId = BuildConfig.UNSPLASH_CLIENT_ID

    val clientSecret = BuildConfig.UNSPLASH_CLIENT_SECRET

    val redirectUri = BuildConfig.PHOTOSPLASH_REDIRECT_URL

    private val scopeList = SCOPES.joinToString(separator = "+")

    val authorizationUrl = "$AUTHORIZATION_URL?client_id=$clientId&redirect_uri=$redirectUri&response_type=$RESPONSE_TYPE&scope=$scopeList"
}
