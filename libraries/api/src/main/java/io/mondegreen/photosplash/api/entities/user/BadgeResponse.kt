package io.mondegreen.photosplash.api.entities.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BadgeResponse(
    @Json(name = "title") val title: String,
    @Json(name = "primary") val primary: Boolean,
    @Json(name = "slug") val slug: String,
    @Json(name = "link") val link: String?
)
