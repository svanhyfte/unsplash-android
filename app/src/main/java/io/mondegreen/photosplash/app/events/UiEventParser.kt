package io.mondegreen.photosplash.app.events

import io.mondegreen.photosplash.app.R
import io.mondegreen.photosplash.core.data.authorization.error.AuthorizationException
import javax.inject.Inject

class UiEventParser @Inject constructor(private val authorizationExceptionParser: AuthorizationExceptionParser) {

    fun parseException(throwable: Throwable) =

        when (throwable) {

            is AuthorizationException -> authorizationExceptionParser.convert(throwable)

            else -> parseGenericEvent()
        }

    private fun parseGenericEvent() = UiEvent(
        type = UiEventType.TOAST,
        messageId = R.string.error_generic
    )
}
