package io.mondegreen.photosplash.app.login

import io.mondegreen.photosplash.app.coreComponent
import io.mondegreen.photosplash.app.login.dagger.DaggerLoginComponent

fun LoginFragment.inject() {

    DaggerLoginComponent.factory()
        .create(this.coreComponent(this.requireContext()))
        .inject(this)
}
