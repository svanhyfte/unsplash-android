package io.mondegreen.photosplash.app.events

import android.content.Context
import android.widget.Toast
import javax.inject.Inject

class UiEventFactory @Inject constructor(private val context: Context) {

    fun showUiEvent(event: UiEvent) {

        when (event.type) {

            UiEventType.TOAST -> {

                produceToast(context, extractMessage(event), Toast.LENGTH_SHORT)
            }

            else -> {

                // In Progress.
                produceToast(context, extractMessage(event), Toast.LENGTH_SHORT)
            }
        }
    }

    private fun produceToast(context: Context, message: String, duration: Int) {

        Toast.makeText(context, message, duration).show()
    }

    private fun extractMessage(event: UiEvent): String {

        if (event.messageId != -1) {

            return context.getString(event.messageId)
        } else {

            return event.message ?: "You forgot to set a resource, kiddo."
        }
    }
}
