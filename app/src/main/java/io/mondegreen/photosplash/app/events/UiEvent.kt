package io.mondegreen.photosplash.app.events

class UiEvent(
    val type: UiEventType,
    val messageId: Int = -1,
    val message: String? = null,
    val positiveButtonCallback: (() -> Unit)? = null,
    val negativeButtonCallback: (() -> Unit)? = null
)

enum class UiEventType {

    DIALOG,

    TOAST,

    SNACKBAR
}
