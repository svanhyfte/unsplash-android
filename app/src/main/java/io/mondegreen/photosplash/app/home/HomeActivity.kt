package io.mondegreen.photosplash.app.home

import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import io.mondegreen.photosplash.app.R
import io.mondegreen.photosplash.core_android.extensions.getViewModel
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: HomeViewModel

    private lateinit var navController: NavController

    private lateinit var drawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbarHome)

        inject()

        viewModel = getViewModel(viewModelFactory)

        drawerToggle = ActionBarDrawerToggle(this, drawerLayoutHome, toolbarHome, R.string.drawer_open, R.string.drawer_close)

        navController = findNavController(R.id.navHostFragment)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayoutHome)
        NavigationUI.setupWithNavController(navigationViewHome, navController)
    }

    override fun onBackPressed() {
        if (drawerLayoutHome.isDrawerOpen(GravityCompat.START)) {

            drawerLayoutHome.closeDrawer(GravityCompat.START)
        } else {

            super.onBackPressed()
        }
    }

    /*
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.setChecked(true)

        drawerLayoutHome.closeDrawers()

        val fragmentId = when (item.getItemId()) {

            R.id.drawer_item_feed -> R.id.feedFragment

            R.id.drawer_item_login -> R.id.loginFragment

            else -> null
        }

        fragmentId?.let { navController.navigate(it) }

        return true
    }
    */
}
