package io.mondegreen.photosplash.app.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import io.mondegreen.photosplash.app.events.UiEvent
import io.mondegreen.photosplash.app.events.UiEventParser
import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.SchedulerProvider
import io.mondegreen.photosplash.core.data.authorization.usecase.DecodeOAuthCodeRequest
import io.mondegreen.photosplash.core.data.authorization.usecase.DecodeOAuthCodeUseCase
import io.mondegreen.photosplash.core.data.authorization.usecase.GetAuthorizationUrlUseCase
import io.mondegreen.photosplash.core.data.authorization.usecase.RequestAuthenticationTokenRequest
import io.mondegreen.photosplash.core.data.login.usecase.AuthenticateGetMeUseCase
import io.mondegreen.photosplash.core.data.user.model.User
import io.mondegreen.photosplash.core_android.lifecycle.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import java.net.URI
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val getAuthorizationUrlUseCase: GetAuthorizationUrlUseCase,
    private val decodeOAuthCodeUseCase: DecodeOAuthCodeUseCase,
    private val authenticateGetMeUseCase: AuthenticateGetMeUseCase,
    private val schedulerProvider: SchedulerProvider,
    private val uiEventParser: UiEventParser
) : ViewModel() {

    private val TAG = javaClass.simpleName

    private val _errorUiEvent = SingleLiveEvent<UiEvent>()

    val getErrorUiEvent: LiveData<UiEvent>
        get() = _errorUiEvent

    private val disposables = CompositeDisposable()

    val authorizationUrl = getAuthorizationUrlUseCase()

    fun onUriReceived(uri: URI?) {

        val codeResult = decodeOAuthCodeUseCase(DecodeOAuthCodeRequest.build(uri?.toString()))

        when (codeResult) {

            is Result.Success -> requestAuthenticationToken(codeResult.data)

            is Result.Error -> handleOAuthDecodingError(codeResult.exception)
        }
    }

    private fun handleOAuthDecodingError(exception: Exception) {

        _errorUiEvent.value = uiEventParser.parseException(exception)
    }

    private fun requestAuthenticationToken(code: String) {

        disposables += authenticateGetMeUseCase(RequestAuthenticationTokenRequest.build(code))
            .subscribeOn(schedulerProvider.io)
            .observeOn(schedulerProvider.ui)
            .subscribe(::onRequestAuthenticationTokenSuccess, ::onRequestAuthenticationTokenFailure)
    }

    private fun onRequestAuthenticationTokenSuccess(result: Result<User>) {

        // Fix.
        Log.e(TAG, "Authentication result is in: $result")

        when (result) {

            is Result.Success -> Log.e(TAG, "Authentication Successful: ${result.data}")
            is Result.Error -> Log.e(TAG, "Authentication Failed", result.exception)
        }
    }

    private fun onRequestAuthenticationTokenFailure(exception: Throwable) {

        _errorUiEvent.value = uiEventParser.parseException(exception)
    }

    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}
