package io.mondegreen.photosplash.app.dagger.components

import android.app.Service
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

interface BaseComponent<T> {

    fun inject(target: T)
}

/**
 * Base dagger component for use in activities.
 */
interface BaseActivityComponent<T : AppCompatActivity> : BaseComponent<T>

/**
 * Base dagger component for use in Fragments.
 */
interface BaseFragmentComponent<T : Fragment> : BaseComponent<T>

/**
 * Base dagger components for use in services.
 */
interface BaseServiceComponent<T : Service> : BaseComponent<T>
