package io.mondegreen.photosplash.app

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import io.mondegreen.photosplash.app.dagger.components.CoreComponent
import io.mondegreen.photosplash.app.dagger.components.DaggerCoreComponent
import timber.log.Timber

class PhotoSplashApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        Timber.plant(Timber.DebugTree())
    }

    private val coreComponent: CoreComponent by lazy {

        DaggerCoreComponent.factory().create(this)
    }

    companion object {
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as PhotoSplashApplication).coreComponent
    }
}

fun FragmentActivity.coreComponent(context: Context) = PhotoSplashApplication.coreComponent(context)

fun Fragment.coreComponent(context: Context) = PhotoSplashApplication.coreComponent(context)
