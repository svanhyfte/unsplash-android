package io.mondegreen.photosplash.app.dagger.components

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.api.dagger.ApiModule
import io.mondegreen.photosplash.api.service.UnsplashService
import io.mondegreen.photosplash.app.dagger.modules.SharedPreferencesModule
import io.mondegreen.photosplash.data.dagger.AuthenticationModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, AuthenticationModule::class, SharedPreferencesModule::class])
interface CoreComponent {

    @Component.Factory interface Factory {

        fun create(@BindsInstance context: Context): CoreComponent
    }

    // TODO: Find a way to get rid of this.
    fun provideUnsplashService(): UnsplashService
    fun provideAuthenticationStore(): AuthenticationTokenStore
    // Provide the context passed to the CoreComponent to satisfy Module dependencies.
    fun provideContext(): Context
}
