package io.mondegreen.photosplash.app.home

import androidx.lifecycle.ViewModel
import io.mondegreen.photosplash.core.data.user.usecase.GetMeUseCase
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val getMeUseCase: GetMeUseCase) : ViewModel()
