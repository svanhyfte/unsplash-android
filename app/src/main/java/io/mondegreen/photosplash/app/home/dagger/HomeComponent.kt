package io.mondegreen.photosplash.app.home.dagger

import dagger.Component
import io.mondegreen.photosplash.app.dagger.components.BaseActivityComponent
import io.mondegreen.photosplash.app.dagger.components.CoreComponent
import io.mondegreen.photosplash.app.dagger.modules.ViewModelModule
import io.mondegreen.photosplash.app.home.HomeActivity
import io.mondegreen.photosplash.core.data.dagger.scopes.FeatureScope
import io.mondegreen.photosplash.data.dagger.DataModule

@Component(
    modules = [ViewModelModule::class, DataModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
interface HomeComponent : BaseActivityComponent<HomeActivity> {

    @Component.Factory
    interface Factory {

        fun create(
            component: CoreComponent
        ): HomeComponent
    }
}
