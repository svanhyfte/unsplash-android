package io.mondegreen.photosplash.app.login.dagger

import dagger.Component
import io.mondegreen.photosplash.app.dagger.components.BaseFragmentComponent
import io.mondegreen.photosplash.app.dagger.components.CoreComponent
import io.mondegreen.photosplash.app.dagger.modules.ViewModelModule
import io.mondegreen.photosplash.app.login.LoginFragment
import io.mondegreen.photosplash.core.data.dagger.scopes.FeatureScope
import io.mondegreen.photosplash.data.dagger.DataModule

@Component(
    modules = [ViewModelModule::class, DataModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
interface LoginComponent : BaseFragmentComponent<LoginFragment> {

    @Component.Factory
    interface Factory {

        fun create(component: CoreComponent): LoginComponent
    }
}
