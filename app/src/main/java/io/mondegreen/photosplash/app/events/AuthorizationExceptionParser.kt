package io.mondegreen.photosplash.app.events

import io.mondegreen.photosplash.app.R
import io.mondegreen.photosplash.core.data.authorization.error.AuthorizationException
import io.mondegreen.photosplash.core.data.authorization.error.AuthorizationExceptionType
import javax.inject.Inject

class AuthorizationExceptionParser @Inject constructor() {

    fun convert(authException: AuthorizationException) = UiEvent(UiEventType.TOAST,
        messageId = when (authException.type) {

            AuthorizationExceptionType.NO_URI -> R.string.error_oauth_no_uri

            AuthorizationExceptionType.NO_CODE -> R.string.error_oauth_no_code

            AuthorizationExceptionType.NO_ACCESS -> R.string.error_oauth_no_access
        })
}
