package io.mondegreen.photosplash.app.home

import io.mondegreen.photosplash.app.coreComponent
import io.mondegreen.photosplash.app.home.dagger.DaggerHomeComponent

internal fun HomeActivity.inject() {

        DaggerHomeComponent.factory()
            .create(coreComponent(this))
            .inject(this)
}
