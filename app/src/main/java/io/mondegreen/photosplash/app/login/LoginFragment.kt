package io.mondegreen.photosplash.app.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import io.mondegreen.photosplash.app.R
import io.mondegreen.photosplash.app.events.UiEventFactory
import io.mondegreen.photosplash.core_android.extensions.getViewModel
import io.mondegreen.photosplash.core_android.extensions.toURI
import io.mondegreen.photosplash.core_android.utils.DeeplinkUtils
import javax.inject.Inject
import kotlinx.android.synthetic.main.fragment_login.*
import timber.log.Timber

class LoginFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var uiEventFactory: UiEventFactory

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inject()

        viewModel = getViewModel(viewModelFactory)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getErrorUiEvent.observe(viewLifecycleOwner, Observer {

            uiEventFactory.showUiEvent(it)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =

        inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()

        hasDeeplinkAuthToken(arguments)
    }

    private fun hasDeeplinkAuthToken(arguments: Bundle?) {

        arguments?.let { args ->

            val uri = DeeplinkUtils.getDeeplinkIntentFromArguments(args)?.data

            viewModel.onUriReceived(uri.toURI())
        }
    }

    private fun setupListeners() {

        buttonLogin.setOnClickListener { openCustomTab(viewModel.authorizationUrl) }
    }

    private fun openCustomTab(url: String) {

        Timber.v("openCustomTab: $url")

        context?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

        /*
        CustomTabsIntent.Builder()
            .setShowTitle(true)
            .build()
            .launchUrl(this.requireContext(), Uri.parse(url))
            */
    }
}
