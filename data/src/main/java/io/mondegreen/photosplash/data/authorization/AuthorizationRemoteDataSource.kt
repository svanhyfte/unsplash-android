package io.mondegreen.photosplash.data.authorization

import io.mondegreen.photosplash.api.authorization.AuthorizationStore
import io.mondegreen.photosplash.api.entities.AuthenticationResponse
import io.mondegreen.photosplash.api.service.UnsplashService
import io.mondegreen.photosplash.core.data.authorization.model.AuthenticationToken
import io.mondegreen.photosplash.core.data.authorization.source.AuthorizationDataSource
import io.mondegreen.photosplash.data.convertToResult
import javax.inject.Inject

private const val GRANT_TYPE = "authorization_code"

class AuthorizationRemoteDataSource @Inject constructor(
    private val unsplashService: UnsplashService,
    private val authorizationStore: AuthorizationStore
) : AuthorizationDataSource {

    override fun requestAuthenticationToken(code: String) =

            unsplashService.getAuthorizationToken(authorizationStore.oAuthTokenUrl, authorizationStore.clientId,
                authorizationStore.clientSecret, authorizationStore.redirectUri, code, "authorization_code")
                .map { it.convertToResult { response -> response.toAuthenticationToken() } }
}

private fun AuthenticationResponse.toAuthenticationToken() = AuthenticationToken(this.accessToken)
