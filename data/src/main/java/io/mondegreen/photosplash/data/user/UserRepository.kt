package io.mondegreen.photosplash.data.user

import io.mondegreen.photosplash.core.data.dagger.scopes.FeatureScope
import io.mondegreen.photosplash.core.data.user.repository.UserRepository as CoreRepository
import io.mondegreen.photosplash.core.data.user.source.UserDataSource
import javax.inject.Inject

@FeatureScope
class UserRepository @Inject constructor(private val userDataSource: UserDataSource) :
    CoreRepository {

    override fun getMe() = userDataSource.getMe()

    override fun getUser(username: String) = userDataSource.getUser(username)
}
