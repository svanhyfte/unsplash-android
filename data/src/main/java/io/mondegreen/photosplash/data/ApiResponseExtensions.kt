package io.mondegreen.photosplash.data

import io.mondegreen.photosplash.api.ApiResponse
import io.mondegreen.photosplash.core.data.Result

internal inline fun <reified T : Any, R : Any> ApiResponse<T>.convertToResult(converter: (T) -> R): Result<R> =

    if (this.isSuccess) {

        Result.Success(converter((this as ApiResponse.Success).data))
    } else {

        Result.Error((this as ApiResponse.Error).exception)
    }
