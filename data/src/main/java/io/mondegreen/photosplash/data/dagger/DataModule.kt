package io.mondegreen.photosplash.data.dagger

import dagger.Binds
import dagger.Module
import io.mondegreen.photosplash.core.data.authorization.repository.AuthorizationRepository as CoreAuthorizationRepository
import io.mondegreen.photosplash.core.data.authorization.source.AuthorizationDataSource
import io.mondegreen.photosplash.core.data.user.repository.UserRepository as CoreUserRepository
import io.mondegreen.photosplash.core.data.user.source.UserDataSource
import io.mondegreen.photosplash.data.authorization.AuthorizationRemoteDataSource
import io.mondegreen.photosplash.data.authorization.AuthorizationRepository
import io.mondegreen.photosplash.data.user.UserRemoteDataSource
import io.mondegreen.photosplash.data.user.UserRepository

@Module
abstract class DataModule {

    @Binds
    abstract fun provideAuthorizationSource(authSource: AuthorizationRemoteDataSource): AuthorizationDataSource

    @Binds
    abstract fun provideAuthorizationRepository(authRepository: AuthorizationRepository): CoreAuthorizationRepository

    @Binds
    abstract fun provideUserSource(userSource: UserRemoteDataSource): UserDataSource

    @Binds
    abstract fun provideUserRepository(userRepository: UserRepository): CoreUserRepository
}
