package io.mondegreen.photosplash.data.authentication

import android.content.SharedPreferences
import androidx.core.content.edit
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.core.data.dagger.qualifiers.Authentication
import javax.inject.Inject

private const val KEY_BEARER_TOKEN = "bearerAccessToken"
private const val KEY_USE_BEARER_TOKEN = "useBearerAccessToken"

class LocalAuthenticationTokenStore @Inject constructor(@Authentication private val sharedPreferences: SharedPreferences) :
    AuthenticationTokenStore {

    private var _bearerAccessToken: String? = sharedPreferences.getString(KEY_BEARER_TOKEN, null)

    private var _useBearerAccessToken: Boolean = sharedPreferences.getBoolean(KEY_USE_BEARER_TOKEN, true)

    override var useBearerToken: Boolean = _useBearerAccessToken
        set(value) {
            sharedPreferences.edit { putBoolean(KEY_USE_BEARER_TOKEN, value) }
            field = value
        }

    override var bearerAccessToken: String? = _bearerAccessToken
        set(value) {
            sharedPreferences.edit { putString(KEY_BEARER_TOKEN, value) }
            field = value
        }

    override fun clearData() {

        sharedPreferences.edit { KEY_BEARER_TOKEN to null }
        _bearerAccessToken = null
    }
}
