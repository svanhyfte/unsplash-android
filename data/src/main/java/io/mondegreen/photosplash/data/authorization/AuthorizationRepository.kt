package io.mondegreen.photosplash.data.authorization

import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.api.authorization.AuthorizationStore
import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.authorization.model.AuthenticationToken
import io.mondegreen.photosplash.core.data.authorization.repository.AuthorizationRepository
import io.mondegreen.photosplash.core.data.authorization.source.AuthorizationDataSource
import javax.inject.Inject

class AuthorizationRepository @Inject constructor(
    private val authorizationDataSource: AuthorizationDataSource,
    private val authorizationStore: AuthorizationStore,
    private val authenticationTokenStore: AuthenticationTokenStore
) : AuthorizationRepository {

    override fun requestAuthenticationToken(code: String) = authorizationDataSource.requestAuthenticationToken(code)
        .map {

            if (it is Result.Success)

                saveAuthenticationToken(it.data)

            it
        }

    override fun getAuthorizationUrl() = authorizationStore.authorizationUrl

    private fun saveAuthenticationToken(token: AuthenticationToken) {

        authenticationTokenStore.bearerAccessToken = token.token
    }
}
