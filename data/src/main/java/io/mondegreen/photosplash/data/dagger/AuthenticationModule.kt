package io.mondegreen.photosplash.data.dagger

import dagger.Binds
import dagger.Module
import io.mondegreen.photosplash.api.authentication.AuthenticationTokenStore
import io.mondegreen.photosplash.data.authentication.LocalAuthenticationTokenStore

@Module
abstract class AuthenticationModule {

    @Binds
    abstract fun provideAuthStore(store: LocalAuthenticationTokenStore): AuthenticationTokenStore
}
