package io.mondegreen.photosplash.data.user

import io.mondegreen.photosplash.api.service.UnsplashService
import io.mondegreen.photosplash.core.data.Result
import io.mondegreen.photosplash.core.data.dagger.scopes.FeatureScope
import io.mondegreen.photosplash.core.data.user.model.User
import io.mondegreen.photosplash.core.data.user.source.UserDataSource
import io.mondegreen.photosplash.data.convertToResult
import io.reactivex.Single
import javax.inject.Inject

@FeatureScope
class UserRemoteDataSource @Inject constructor(private val unsplashService: UnsplashService) : UserDataSource {

    override fun getMe(): Single<Result<User>> = unsplashService.getMe().map { it.convertToResult { userResponse -> userResponse.toUser() } }

    override fun getUser(username: String) = unsplashService.getUser(username).map { it.convertToResult { userResponse -> userResponse.toUser() } }
}
