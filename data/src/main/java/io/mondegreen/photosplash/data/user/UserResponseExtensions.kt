package io.mondegreen.photosplash.data.user

import io.mondegreen.photosplash.api.entities.user.BadgeResponse
import io.mondegreen.photosplash.api.entities.user.UserResponse
import io.mondegreen.photosplash.core.data.user.model.Badge
import io.mondegreen.photosplash.core.data.user.model.User

fun UserResponse.toUser() = User(
    id = id,
    userName = username,
    firstName = firstName,
    lastName = lastName,
    twitterName = twitterName,
    portfolioUrl = portfolioUrl,
    bio = bio,
    location = location,
    totalLikes = totalLikes,
    totalPhotos = totalPhotos,
    totalCollections = totalCollections,
    isFollowedByUser = followedByUser,
    downloads = downloads,
    instagramUsername = instagramUsername,
    email = email,
    links = links,
    profileImages = profileImages,
    badge = badge?.toBadge()
)

fun BadgeResponse.toBadge() = Badge(
    title = title,
    isPrimary = primary,
    slug = slug,
    link = link ?: ""
)
